import CartParser from './CartParser';
import cart from '../samples/cart.json';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it("Parse function to parse a file into items and total", () => {
		//console.log(text);
		expect(parser.parse('D:/study/binaryAcademy/testing/BSA2021-Testing/samples/cart.csv')).toEqual(expect.objectContaining({
			items: expect.any(Array),
			total: expect.any(Number),
		  }));
	});
	it("Parse Line to object", () => {
		expect(parser.parseLine('Mollis consequat,9.00,2')).toEqual(expect.objectContaining({
			"id": expect.any(String),
            "name": "Mollis consequat",
            "price": 9,
            "quantity": 2
		}));
	});
	it("Make right Error message", () => {
		expect(parser.createError('type', 'row', 'column', 'message')).toEqual(expect.objectContaining({
			type: 'type',
			row: 'row',
			column: 'column',
			message: 'message'
		}));
	});
	it("Calct Total", () => {
		expect(parser.calcTotal(cart.items)).toBeCloseTo(348.32);
	});
	it("Validate Header Text", () => {
		expect(parser.validate(`TestProduct name,Price,Quantity
		Mollis consequat,9.00,2`)).toEqual(expect.arrayContaining([expect.objectContaining({
			"column": expect.any(Number),
			"message": expect.stringContaining(`Expected header to be named \"Product name\" but received TestProduct name.`),
			"row": expect.any(Number),
			"type": expect.stringContaining("header")
		})]));
	});
	it("Validate columns length", () => {
		expect(parser.validate(`Product name,Price,
		Mollis consequat,9.00,2`)).toEqual(expect.arrayContaining([expect.objectContaining({
			"column": expect.any(Number),
			"message": expect.stringContaining(`Expected header to be named \"Quantity\" but received .`),
			"row": expect.any(Number),
			"type": expect.stringContaining("header")
		})]));
	});
	it("Validate cell to be not empty", () => {
		expect(parser.validate(`Product name,Price,Quantity
		Mollis consequat,9.00`)).toEqual(expect.arrayContaining([expect.objectContaining({
			"column": expect.any(Number),
			"message": expect.stringContaining(`Expected row to have 3 cells but received 2.`),
			"row": expect.any(Number),
			"type": expect.stringContaining("row")
		})]));
	});
	it("Validate cell to be positive", () => {
		expect(parser.validate(`Product name,Price,Quantity
		Mollis consequat,9.00,-2`)).toEqual(expect.arrayContaining([expect.objectContaining({
			"column": expect.any(Number),
			"message": expect.stringContaining(`Expected cell to be a positive number but received \"-2\".`),
			"row": expect.any(Number),
			"type": expect.stringContaining("cell")
		})]));
	});
	it("Validate cell to be positive", () => {
		expect(parser.validate(`Product name,Price,Quantity
		Mollis consequat,9.00,test`)).toEqual(expect.arrayContaining([expect.objectContaining({
			"column": expect.any(Number),
			"message": expect.stringContaining(`Expected cell to be a positive number but received \"test\".`),
			"row": expect.any(Number),
			"type": expect.stringContaining("cell")
		})]));
	});


});

describe('CartParser - integration test', () => {
	// Add your integration test here.
});